//
// Created by 童仕龙 on 2024/2/19.
//

/*
 * 算术运算符
 *
 */

#include <stdio.h>
#include <stdlib.h>

void test_suanshu(int a, int b) {    //算术运算符
    printf("%d\n", a + b);
    printf("%d\n", a - b);
    printf("%d\n", a * b);
    printf("%d\n", a / b);
    printf("%d\n", a % b);
    printf("%d\n", a++);
    printf("%d\n", a--);
    printf("%d\n", --a);
    printf("%d\n", ++a);
}

void test_guanxi(int a, int b) {
    if (a == b) {
        printf("a等于b\n");
    }
    if (a != b) {
        printf("a不等于b\n");
    }
    if (a > b) {
        printf("a大于b\n");
    }
    if (a < b) {
        printf("a小于b\n");
    }
    if (a >= b) {
        printf("a大于等于b\n");
    }
    if (a <= b) {
        printf("a小于等于b\n");
    }
}

void test_luoji(int a, int b) {  //逻辑运算符
    while (a > 0 && b > 0) {
        if (a>b && a%b==0){
            printf("a能整除b\n");
        }
        else{
            printf("a不能整除b\n");
        }
    }
}


int main() {
    int a;
    a = 'A' + 1.6;
    printf("%d\n", a);
    return 0;
}