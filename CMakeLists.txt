cmake_minimum_required(VERSION 3.26)
project(C_Study C)

set(CMAKE_C_STANDARD 11)

include_directories(.)

add_executable(C_Study
        helloworld.c
        tempTransfer.c
        基本数据类型.c
        常量&变量.c
        运算符.c
        输出.c
        循环.c
)
