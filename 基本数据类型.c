//
// Created by 童仕龙 on 2024/2/18.
//

/*
 * 基本数据类型：整型、浮点型、char字符型
 * C语言中没有布尔类型，可以使用整型0和1来表示布尔值false和true。
 * 整型变量默认为0，浮点型变量默认为0.0，char字符型变量默认为'\0'。
 * 'A' 表示单个字符大写字母A，占用1个字节空间
    "A" 表示字符串，该字符串只有1个大写字母A组成，占用2个字节空间，每个字符串末尾自动会加上一个空字符 '\0'
    空字符常量使用转义符号 '\0'表示，空白字符串使用双引号表示 ""

 * 浮点型
 */
#include "stdio.h"

int test_int() {
    int a = 10;
    int b = 3;
    int c = a + b;
    printf("a + b = %d\n", c);
    return 0;
}

int test_float() {
    float a = 10.5;
    float b = 3.5;
    float c = a + b;
    printf("a + b = %f\n", c);
    return 0;
}

int test_char() {
    char char01= 'A';
    printf("%d",char01);
    return 0;
 }

 int main() {
    test_char();
}