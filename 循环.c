//
// Created by 童仕龙 on 2024/2/20.
//
#include "stdio.h"

int test_for(int num) {
    int sum;
    for (int i = 0; i < num; i++) {
        int  score;
        printf("请输入第%d位同学的分数：", i + 1);
        scanf("%d", &score);
        sum += score;
    }
    return sum;
}

int test_while(int num) {
    int sum;
    int i = 0;
    while (i < num) {
        int score;
        printf("请输入第%d位同学的分数：", i + 1);
        scanf("%d", &score);
        sum += score;
        i++;
    }
    return sum;
}

int main() {
    int res = test_for(3);
    printf("总分：%d", res);
}