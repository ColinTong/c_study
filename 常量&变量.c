//
// Created by 童仕龙 on 2024/2/18.
//
/*
 * 常量：程序运行过程中，其值不能被修改的量
 *
 * 常量包括：
 *      字面量：程序源代码中一个固定的值，如 520，'A'，3.14159
 *      符号常量：由 # define 定义的标识符，表示程序中一个固定值，好处是在预处理阶段 就将宏定义的值替换为最终值，不需要创建存储空间
 *
 * 其中字面量又包括：整型、浮点型、字符型、字符串型
 *
 *
 * 变量：用来保存一些特殊内容，在程序执行过程中随时会变化的值
 * 定义：[存储类型] 数据类型  标识符 = 值   (存储类型可以省略)
 * 标识符：由字母、数字、下划线组成，不能以数字开头的字符序列
 * 数据类型：基础数据类型 / 构造数据类型
 * 存储类型：auto   static  register   extern(说明型)
 *      auto：默认，自动分配空间，自动回收空间
 *      static：（静态型）静态存储方式，程序结束后由系统释放空间；自动初始化为0值或空值，并且变量的值有继承性；另外常用于修饰变量或函数
 *      register：（建议型）寄存器存储方式，程序结束后由系统释放空间，只能定义局部变量，不能定义全局变量；大小有限制，不能超过寄存器位数；寄存器没有地址，所以寄存器变量无法打印地址或查看
 *      extern：（说明型）外部的，全局变量，在程序中可以被多个源文件访问，不能改变被说明的变量的值或类型
 *
 * 全局变量 & 局部变量
 *      全局变量：在函数体外定义的变量，在程序中任何地方都能访问
 *      局部变量：在函数体内定义的变量，在函数体内能被访问
 */


#include <stdio.h>
#define PI 3.14159
#define ADD (2+3)
#define  MAX(a, b) ((a) > (b) ? (a) : (b))

void test_constant()    //常量
{
    int a = 10;
    float b = 3.14;
    char ch = 'A';
    printf("a = %d, b = %f, ch = %c, %f\n", a, b, ch, PI);
}

void test_static()  //static静态变量
{
    static int s = 0;   // static 修饰的变量，在程序运行期间，不会重新计算初始值
    s++;
    printf("s = %d\n", s);
}

void test_local_var()   //局部变量
{

}
int main()
{
    printf("int 存储大小 : %lu \n", sizeof(int));
    printf("%.2f\n", PI);
    char ch = 'A';
    int *p = (int *) &ch;
    printf("%d\n", *p);

    test_static();
    test_static();
    test_static();
    return 0;


}