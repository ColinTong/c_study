//
// Created by 童仕龙 on 2024/2/20.
// 输入输出相关内容
/*
 * 标准IO、文件IO
 *
 * 1.格式化输入输出：scanf、printf
 * 2.字符输入输出：getchar、putchar
 * 3.字符串输入输出：gets、puts
 *
 * printf("%[修饰符]格式字符串", 参数列表)
 *
    ％d			//整型输出
    ％ld		//长整型输出
    ％o			//以八进制数形式输出整数
    ％x		   //以十六进制数形式输出整数，或输出字符串的地址
    ％u			//以十进制数输出unsigned型数据(无符号数)
    注意：		%d与%u的区别是，有无符号的数值范围不同，也就是极限的值不同，不然数值打印出来会有误
    ％c			//用来输出一个字符
    ％s			//用来输出一个字符串
    ％f			//用来输出实数，以小数形式输出，默认情况下保留小数点6位
    %.5f		//用来输出实数，保留小数点5位
    ％e			//以指数形式输出实数
    ％g			//根据大小自动选f格式或e格式，且不输出无意义的零
    %l          // 修饰 d、o、u时，表示long数据类型； 修饰 f、e时，表示double数据类型
 *
 *
 *
 */

#include "stdio.h"
#define  MAX (60LL * 60LL * 24LL * 365LL)   //LL修饰数据，表示将int类型数据转换为long long类型数据

int main() {
    int a = 123, b = 123456;
    printf("%5d\n", a);        // 默认右对齐，且最少取5位整数，多余5位全取，不足5位使用空格左面补全
    printf("%05d\n", a);    // 使用0代替空格，在左边补齐位数
    printf("%-5d\n", a);    // 左对齐，不足位数，使用空格补全
    printf("%-05d\n", a);    // 左对齐，不足位数，还是用空格补全
    printf("%5d\n", b);        // 超过5位全取

    double c = 123.326, d = 90.12;
    printf("%.2f\n", c); // 取2位小数，且第三位四色五入
    printf("%.3f\n", d); // 取3位小数，且不足的用0补全
    printf("%4.2f\n", d); // 取至少4位字符，2位从b的左边开始取2位，剩余2位<整数2位+小数点1位,所以3位全部输出
    printf("%7.2f\n", d); // 取至少7位字符， 2位小数，剩余5位>整数2位 + 小数点1位， 多出来的2位用空格补全

    char str[30];
    char s1[5] = {'a', 'b', 'c'};
    printf("%s==\n", s1);                // 打印完整的字符数组（字符串）
    printf("%2s==\n", s1);                // 打印至少2个字符
    printf("%5s==\n", s1);                // 打印至少5个字符，不足的用空格在左边补齐
    printf("%-5s==\n", s1);                // 打印至少5个字符，不足的用空格在右边补齐
    printf("%4.2s==\n", s1);            // 总共输出4个字符，但是有2个需要在s1里面从左取，剩余的字符用空格默认在左边补全
    printf("%.2s==\n", s1);                // 总共输出2个字符，这2个字符从s1里面的左边开始取

    return 0;
}
